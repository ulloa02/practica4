<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Delimitadores de código PHP</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="css/tabs.css" />
		<link rel="stylesheet" href="css/delimeters.css" />
	</head>
	<body>
	<header>
		<h1>Los delimitadores de codigo PHP</h1><br>
	</header>
	<section>
		<article>
			<div class="contenedor-tabs">
				<?php
					echo "<span class=\"diana\" id=\"una\"></span>\n";
					echo "<div class=\"tab\">\n";
					echo "<a href=\"#una\" class=\"tab-e\">Estilo XML</a>\n";
					echo "<div class=\"first\">\n";
					echo "<p class=\"xmltag\">\n";
					echo "Este texto esta escrito en PHP, utilizando las etiquetas mas";
					echo " usuales y recomendadas para delimitar el codigo PHP, que son: ";
					echo " &lt;?php ... ?&gt;.<br>\n";
					echo "</p>\n";
					echo "</div>\n";
					echo "</div>\n";
					?>
						<script language-"php">
							echo "<span class=\"diana\" id=\"dos\"></span>\n";
							echo "<div class=\"tab\">\n";
							echo "<a href=\"#dos\" class=\"tab-e\">Script</a>\n";
							echo "<div>\n";
							echo "<p class=\"htmltag\">\n";
							echo "A pesar de que estas lineas estan escritas dentro de un script php";
							echo " Estan enmarcadas dentro de etiquetas HTML: ";
							echo " &lt;?script&gt;... &lt;/script&gt;";
							echo "</p>\n";
							echo "</div>\n";
							echo "</div>\n";
						</script>
					<?
						echo "<span class=\"diana\" id=\"tres\"></span>\n";
						echo "<div class=\"tab\">\n";
						echo "<a href=\"#tres\" class=\"tab-e\">Etiquetas cortas</a>\n";
						echo "<div>\n";
						echo "<p class=\"shorttag\">\n";
						echo "Este texto tambien esta escrito con php, utilizando las etiquetas ";
						echo " cortas, <br>\n que son: &lt;? ... ?&gt;";
						echo "</p>\n";
						echo "</div>\n";
						echo "</div>\n";
					?>
					<% 
						echo "<span class=\"diana\" id=\"cuatro\"></span>\n";
						echo "<div class=\"tab\">\n";
						echo "<a href=\"#cuatro\" class=\"tab-e\">Etiquetas ASP</a>\n";
						echo "<div>\n";
						echo "<p class=\"asptag\">";
						echo "Este texto esta escrito en PHP, como los dos ejemplos anteriores. ";
						echo "sin embargo, se ha delimitado con etiquetas estilo asp: ";
						echo "&lt;% ... %&gt;.<br>\n";
						echo "</p>\n";
						echo "</div>\n";
						echo "</div>\n";
					%>
				</div>
			</article>
		</section>
		</body>
	</html>
